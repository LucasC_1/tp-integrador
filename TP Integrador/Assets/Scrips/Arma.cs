using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class Arma : MonoBehaviour
{
    public Transform puntoDeDisparo;
    public GameObject balaPrefab;
    public float velocidadBala = 10f;
    public float tiempoDeVida = 3f;

    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            Disparar();
        }
    }

    void Disparar()
    {
        if (balaPrefab != null && puntoDeDisparo != null)
        {
            GameObject nuevaBala = Instantiate(balaPrefab, puntoDeDisparo.position, puntoDeDisparo.rotation);

            Rigidbody rbBala = nuevaBala.GetComponent<Rigidbody>();

            // Configura la velocidad de la bala
            rbBala.velocity = puntoDeDisparo.forward * velocidadBala;

            // Destruye la bala despu�s de un tiempo
            Destroy(nuevaBala, tiempoDeVida);
        }
        else
        {
            UnityEngine.Debug.LogError("Prefab de bala o punto de disparo no asignado en el Inspector.");
        }
    }
}
