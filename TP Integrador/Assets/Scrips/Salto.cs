using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Salto : MonoBehaviour
{
    public float fuerzaSalto = 10f;
    public int maxSaltos = 2;
    private int saltosRestantes;

    private Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        saltosRestantes = maxSaltos;
    }

    void Update()
    {
        if (Input.GetButtonDown("Jump") && saltosRestantes > 0)
        {
            Saltar();
        }
    }

    void FixedUpdate()
    {
        // Puedes agregar aqu� tu l�gica de movimiento horizontal si es necesario.
    }

    void Saltar()
    {
        rb.velocity = new Vector3(rb.velocity.x, 0f, rb.velocity.z);
        rb.AddForce(Vector3.up * fuerzaSalto, ForceMode.Impulse);
        saltosRestantes--;
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Suelo"))
        {
            saltosRestantes = maxSaltos;
        }
    }
}
