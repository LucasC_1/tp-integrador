using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Meta : MonoBehaviour
{
    public Text mensajeUIText;
    private bool metaAlcanzada = false;

    void Start()
    {
        mensajeUIText.gameObject.SetActive(false); // Desactiva el objeto de texto al inicio
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Personaje") && !metaAlcanzada)
        {
            mensajeUIText.gameObject.SetActive(true); // Activa el objeto de texto
            mensajeUIText.text = "¡Enhorabuena, ganaste!";
            Invoke("ReiniciarNivel", 2f);
            metaAlcanzada = true; // Evita que el mensaje se muestre repetidamente
        }
    }

    private void ReiniciarNivel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
