using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camara : MonoBehaviour
{
    public Transform objetivo; // El objeto a seguir
    public float velocidadRotacion = 2.0f;
    public Vector3 offset; // Distancia entre la c�mara y el objetivo

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked; // Bloquea el cursor
        Cursor.visible = false; // Oculta el cursor
    }

    void LateUpdate()
    {
        float horizontal = Input.GetAxis("Mouse X") * velocidadRotacion;
        objetivo.Rotate(0, horizontal, 0);

        float vertical = Input.GetAxis("Mouse Y") * velocidadRotacion;
        transform.Rotate(-vertical, 0, 0);

        transform.position = objetivo.position - (objetivo.rotation * offset);
    }
}