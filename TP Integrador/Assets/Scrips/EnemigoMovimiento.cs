using System;
using System.Collections;
using UnityEngine;

public class EnemigoMovimiento : MonoBehaviour
{
    bool haSidoDisparado = false;
    Rigidbody rb;
    public float velocidad = 5f; // Puedes ajustar la velocidad seg�n tus necesidades

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        // Llama a la funci�n para mover al enemigo de manera aleatoria
        StartCoroutine(MoverAleatoriamente());
    }

    void Update()
    {
        if (haSidoDisparado)
        {
            // Utiliza UnityEngine.Debug en lugar de solo Debug
            UnityEngine.Debug.Log("Enemigo golpeado y quieto.");
        }
    }

    void OnTriggerEnter(Collider other)
    {
        ManejarColision(other);
    }

    void OnCollisionEnter(Collision collision)
    {
        ManejarColision(collision.collider);
    }

    void ManejarColision(Collider collider)
    {
        if (collider.CompareTag("PlayerBala"))
        {
            haSidoDisparado = true;
            // Utiliza UnityEngine.Debug en lugar de solo Debug
            UnityEngine.Debug.Log("Enemigo golpeado por una bala");
            // Coloca aqu� la l�gica para que el enemigo desaparezca (por ejemplo, desactivando el GameObject).
            gameObject.SetActive(false);
        }
    }

    IEnumerator MoverAleatoriamente()
    {
        while (!haSidoDisparado)
        {
            // Genera una direcci�n aleatoria en el plano XY
            Vector2 direccionAleatoria = UnityEngine.Random.insideUnitCircle.normalized;

            // Convierte la direcci�n a un vector tridimensional
            Vector3 direccionMovimiento = new Vector3(direccionAleatoria.x, 0, direccionAleatoria.y);

            // Aplica el movimiento utilizando el Rigidbody
            rb.velocity = direccionMovimiento * velocidad;

            // Espera un tiempo antes de cambiar la direcci�n
            yield return new WaitForSeconds(UnityEngine.Random.Range(1f, 3f));
        }
    }
}
