using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigo : MonoBehaviour
{
    bool haSidoDisparado = false;

    void Update()
    {
        if (haSidoDisparado)
        {
            // Utiliza UnityEngine.Debug en lugar de solo Debug
            UnityEngine.Debug.Log("Enemigo golpeado y quieto.");
        }
    }

    void OnTriggerEnter(Collider other)
    {
        ManejarColision(other);
    }

    void OnCollisionEnter(Collision collision)
    {
        ManejarColision(collision.collider);
    }

    void ManejarColision(Collider collider)
    {
        if (collider.CompareTag("PlayerBala"))
        {
            haSidoDisparado = true;
            // Utiliza UnityEngine.Debug en lugar de solo Debug
            UnityEngine.Debug.Log("Enemigo golpeado por una bala");
            // Coloca aqu� la l�gica para que el enemigo desaparezca (por ejemplo, desactivando el GameObject).
            gameObject.SetActive(false);
        }
    }
}
