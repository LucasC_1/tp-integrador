using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reinicio : MonoBehaviour
{
    // Puedes ajustar estas variables seg�n tus necesidades
    public Transform jugador; // Referencia al objeto del jugador
    public Vector3 posicionInicial; // Posici�n inicial del jugador

    void Start()
    {
        // Guarda la posici�n inicial del jugador al iniciar el juego
        posicionInicial = jugador.position;
    }

    void Update()
    {
        // Reinicia el juego al presionar la tecla "R"
        if (Input.GetKeyDown(KeyCode.R))
        {
            ReiniciarJuego();
        }

        // Reinicia el juego si el jugador cae del escenario (puedes ajustar el valor -10 seg�n tu escenario)
        if (jugador.position.y < -10)
        {
            ReiniciarJuego();
        }
    }

    void ReiniciarJuego()
    {
        // Repositiona al jugador a la posici�n inicial
        jugador.position = posicionInicial;

        // Puedes agregar m�s l�gica de reinicio seg�n tus necesidades
        // Reiniciar puntuaci�n, restablecer objetos, etc.
    }
}
