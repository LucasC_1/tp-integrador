using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SueloTrampa : MonoBehaviour
{
    public float tiempoInactividad = 2f; // Tiempo en segundos que el suelo estar� inactivo
    public float tiempoReaccion = 1.5f; // Tiempo en segundos para que el jugador reaccione antes de activar la trampa
    private bool activo = true; // Estado actual del suelo

    private void OnTriggerEnter(Collider other)
    {
        // Verificar si el objeto que colisiona es el jugador y la trampa est� activa
        if (other.CompareTag("Personaje") && activo)
        {
            // Invocar el m�todo para activar la trampa despu�s de un tiempo
            Invoke("ActivarTrampa", tiempoReaccion);
        }
    }

    private void ActivarTrampa()
    {
        // Desactivar el suelo y programar su reactivaci�n despu�s de un tiempo
        activo = false;
        gameObject.SetActive(false);
        Invoke("ReactivateFloor", tiempoInactividad);
    }

    private void ReactivateFloor()
    {
        // Reactivar el suelo despu�s del tiempo especificado
        gameObject.SetActive(true);
        activo = true;
    }
}
